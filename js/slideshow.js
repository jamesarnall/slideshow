
/*

=============================================================================
SlideShow
Last edited 24 April 2014 by James Arnall <james@jamesarnall.com>
=============================================================================

TODO: Refactor increment code (call out function)
TODO: Change slider position/value when incrementing/decrementing with keys.
TODO: Why does mouse still retain control after drag?

*/

// --------------
// APP VARIABLES
//---------------


var PF = PF || {};		// Global namespace

PF.Frames = [];                 // List of all image names, in order

PF.CurrentFrame = 0;            // Current frame #


// --------------






// Name calculator function ---------------
PF.GetImageName = function(num) {
  var num = num || 0;
  var s = num.toString();
  var digits = (4 - s.length);
  for (var i = 0; i < digits; i++) {
    s = "0" + s;
  };
  return s;
}

// TODO: Functions to get next/prev numbers
PF.MoveForward = function(num) {
	return (num < (this.ImageCount-1) ? ++num : 0) ;
}

PF.MoveBack = function(num) {
  return (num > 0 ? --num : (this.ImageCount-1)) ;
}

// TODO: Capture mouse input to show help
	// FADE IN MESSAGE, FADE OUT AFTER 5 seconds

PF.Init = function(ct) {
  
  this.ImageCount = ct;   // Number of image frames

  // Build list of images
  for (var i = 0; i < ct; i++) {
    this.Frames[i] = this.GetImageName(i+1);
  }

  this.PictureFrame = 
    this.PictureFrame || 
    document.getElementById("MainFrame").style.backgroundImage;

  var img = new Image();
  for (var i = 0; i < this.Frames.length; i++) {
    img.src = PF.FormatURL(PF.Frames[i]);
  };
  img = null;


}



PF.FormatURL = function(img) {
  return 'images/' + img + '.jpg';
}

PF.ChangeImage = function(frameId) {
    document.getElementById(frameId).style.backgroundImage =
        'url(' + PF.FormatURL(PF.Frames[PF.CurrentFrame]) + ')';
}

PF.ChangeCounter = function(counterId) {
    document.getElementById(counterId).innerHTML =
        PF.Frames[PF.CurrentFrame];
}

PF.Keys = function(e) {

    switch (e.keyCode) {

      case 37: case 40:
        PF.CurrentFrame =
          PF.MoveBack(PF.CurrentFrame);
        PF.ChangeImage('MainFrame');
        PF.ChangeCounter('Counter');
        if (PF.$Shuttle) {
          PF.$Shuttle.slider( "option", "value", PF.CurrentFrame );
        }
        break;

      case 39: case 38:
        PF.CurrentFrame =
          PF.MoveForward(PF.CurrentFrame);
        PF.ChangeImage('MainFrame');
        PF.ChangeCounter('Counter');
        if (PF.$Shuttle) {
          PF.$Shuttle.slider( "option", "value", PF.CurrentFrame );
        }
        break;

      default:
        break;

    }
  }

