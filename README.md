# SlideShow

README last edited 19 April 2014 by James Arnall <james@jamesarnall.com>


## Usage

1. Add images to "images/" directory, numbered sequentially starting with <1>. Pad with leading zeros to make sorting work.
(e.g. If there are 1,000 images, start with "0001.jpg", "0002.jpg"..."1000.jpg")
2. Initialize Init function with the total number of images.
3. Open INDEX.HTML